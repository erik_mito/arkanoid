﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumMeteor : TinyMeteor
{


    protected override void Explode()
    {
        MeteorManager.instance.LaunchMeteor(1, transform.position, new Vector2(2, -2), 20);
        MeteorManager.instance.LaunchMeteor(1, transform.position, new Vector2(-2, -2), 20);
        base.Explode();
    }
}

