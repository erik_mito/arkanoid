﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpaceManager : MonoBehaviour {

    public static SpaceManager instance;
    private int highscore;
    private int lifeplayer;
    public Text highscoreText;
    public Text lifeText;
    public GameObject pause;
    void Start()
    {
        instance = this;
        highscore = 0;
        lifeplayer = 3;
        lifeText.text = "x " +lifeplayer.ToString();
        highscoreText.text = highscore.ToString("D5");
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pause.SetActive(!pause.active);
            if(pause.active == true)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }
    public void AddHighscore(int value)
    {
        highscore += value;
        highscoreText.text = highscore.ToString("D5");
    }

    public void Sublife()
    {
        if(lifeplayer > 0) { 
        lifeplayer--;
        lifeText.text = "x "+lifeplayer.ToString();
        }
    }

}
