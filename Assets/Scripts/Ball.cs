﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public Vector2 speed;
    public float maxVel;
    public Vector3 position = new Vector3(0, 0, 0);
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        speed.x *= 1.25f;
        speed.y *= 1.25f;
        if(speed.x > maxVel)
        {
            speed.x = maxVel;
        }
        if(speed.y > maxVel)
        {
            speed.y = maxVel;
        }
        if(speed.x < -maxVel)
        {
            speed.x = -maxVel;
        }
        if(speed.y < -maxVel)
        {
            speed.y = -maxVel;
        }
        switch(other.tag)
        {
            case "Player":
                speed.y = -speed.y;
                break;
            case "top":
                speed.y = -speed.y;
                break;
            case "left":
                speed.x = -speed.x;
                break;
            case "right":
                speed.x = -speed.x;
                break;
            case "bottom":
                transform.position = position;
                speed.y = -speed.y;
                break;
            case "brick":
                other.gameObject.GetComponent<Brick>().Touch();
                speed.y = -speed.y;
                break;
        }
    }
}
