﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {
    private Vector3 vec;
    public float speed;
    protected bool shooting;
    protected Vector3 posIni;
    public AudioSource AudioShoot;
    // Use this for initialization
    protected void Start()
    {
        posIni = transform.position;
    }

    // Update is called once per frame
    protected void Update()
    {
        if(shooting) { 
        transform.Translate(0, speed * Time.deltaTime, 0);
        }
        /*if(transform.position.y > 9.0f)
        {
            Destroy(gameObject);
        }*/
    }
    public void Shoot(Vector3 location, float direction)
    {
        AudioShoot.Play();
        transform.position = location;
        transform.rotation = Quaternion.Euler(0, 0, direction);
        shooting = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Finish" || collision.tag == "Meteor")
            {
            transform.position = posIni;
            shooting = false;
            }
    }
    public void Reset()
    {
        transform.position = posIni;
        shooting = false;
    }
}

