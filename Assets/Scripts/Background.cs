﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {
    private Material mat;
    private Vector2 vec;
    public float speed;
    // Use this for initialization
	void Awake () {
        mat=GetComponent<MeshRenderer>().material;

	}
	
	// Update is called once per frame
	void Update () {
        vec.y += speed * Time.deltaTime;
        if(vec.y > 100.0f)
        {
            vec.y -= 100.0f;
        }
        mat.SetTextureOffset("_MainTex",vec);
	}
}
