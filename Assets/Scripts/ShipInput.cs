﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour {
    public Vector2 axis;
    public PlayerBehaviour player;
    public Weapon weapon;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update () {
        
        axis.y = Input.GetAxis("Vertical");
        
            axis.x = Input.GetAxis("Horizontal");
       
        player.setAxis(axis);
        if(Input.GetButton("Jump"))
        {
            weapon.ShootWeapon();
        }
        if(Input.GetKeyDown(KeyCode.Z))
        {
            weapon.NextWeapon();
        }

    }
}
