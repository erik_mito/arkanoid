﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {
    private bool canShoot;
    private float elapsedTime;
    public float carencia;

    public int numShoot;
    private int actualshot=0;
    // Use this for initialization
    void Awake()
    {
        canShoot = true;
        elapsedTime = 0;
        actualshot = 0;
    }
    private void Update()
    {
        if(!canShoot)
        {
            elapsedTime += Time.deltaTime;
        }
        if(elapsedTime > carencia)
        {
            canShoot = true;
            elapsedTime = 0;
        }
    }
    public void ShootCannon (Shot shot, Vector3 position,float rotation) {
        if(canShoot) { 
            canShoot = false;
            shot.Shoot(position,rotation);
        }
    }
    public bool CanShoot()
    {
        return canShoot;
    }
}
