﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentedBullet : Shot
{

    public int maxAmmo = 2;
    Cartridge cartridge;
    public int timeToExplode = 1;
    private float currentTime = 0f;

    protected new virtual void Start()
    {
        base.Start();
        cartridge = GameObject.Find("Weapon").GetComponent<Weapon>().carts[0];
    }

    protected new virtual void Update()
    {
        base.Update();
        if(shooting)
        {
            currentTime += Time.deltaTime;
            if(currentTime > timeToExplode)
            {
                ShotMiniBullets();
            }
        }
        else
        {
            currentTime = 0f;
        }
    }

    void ShotMiniBullets()
    {
        Debug.Log("Shot mini bullets");
        float z = transform.rotation.eulerAngles.z;
        cartridge.GetShoot().Shoot(transform.position, -45 + z);
        cartridge.GetShoot().Shoot(transform.position, 45 + z);
        Reset();
    }
}
