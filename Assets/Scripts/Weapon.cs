﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    public Cartridge[] carts;
    public Cannon can;
    public int numShoot;
    public Transform posIni;
    public int currentcart=0;

    void Start()
    {
        GameObject[] shot = Resources.LoadAll<GameObject>("Prefab/Bullets");
        carts = new Cartridge[shot.Length];
        for(int i = 0; i < shot.Length; i++)
        {
            carts[i] = new Cartridge(shot[i], posIni, numShoot);
            posIni.Translate(0,1,0);
        }
    }

        public void ShootWeapon()
    {
        if(can.CanShoot()) {
            can.ShootCannon(carts[currentcart].GetShoot(), transform.position, 0f);
        }
   
    }
    public void NextWeapon()
    {
        currentcart++;
        if(currentcart> carts.Length-1)
        {
            currentcart = 0;
        }
       
    }
}
