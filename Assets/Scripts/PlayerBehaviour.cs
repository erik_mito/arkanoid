﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public Vector2 axis;
    public float speed;
    private Vector3 positionActual;
    public float pos=8.5f;
    public Propeller prop;
    public GameObject graphics;
    public AudioSource audio;
    public ParticleSystem explosion;
    private BoxCollider2D mycollider;
    // Use this for initialization
    void Awake()
    {
        mycollider = GetComponent<BoxCollider2D>();
    }
    // Update is called once per frame

    void Update()
    {
        transform.Translate(axis * speed * Time.deltaTime);
        if(axis.y > 0f)
        {
            prop.Blue();
        }
        else if(axis.y < 0f)
        {
            prop.Red();
        }
        else if(axis.y == 0f)
        {
            prop.Stop();
        }
    }
    public void setAxis(Vector2 axisNow)
    {
        positionActual = transform.position;
        if(transform.position.x > pos)
        {
            positionActual.x = pos;
            transform.position = positionActual;

        }
       else if(transform.position.x < -pos)
        {
            positionActual.x = -pos;
            transform.position = positionActual;

        }
        if(transform.position.y > 4.6f)
        {
            positionActual.y = 4.6f;
            transform.position = positionActual;

        }
        else if(transform.position.y < -4.6f)
        {
            positionActual.y = -4.6f;
            transform.position = positionActual;

        }

        axis = axisNow;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag== "Meteor")
        {
            Explode();
        }
    }
    private void Explode() {
        SpaceManager.instance.Sublife();
        graphics.SetActive(false);
        mycollider.enabled = false;
        explosion.Play();
        audio.Play();
        Invoke("Reset", 2);
        }
    private void Reset()
    {
        graphics.SetActive(true);
        mycollider.enabled = true;
    }
}
