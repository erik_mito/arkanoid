﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Brick : MonoBehaviour {
    public int lifes;
    public Material red;
    public Material blue;
    public Material green;
    // Use this for initialization
    void Start() {

    }
    private void Awake()
    {
        ChangeColor();
    }
    private void ChangeColor()
    {
        if(lifes == 3)
        {
            GetComponent<Renderer>().material = red;
        }
        else if(lifes == 2)
        {
            GetComponent<Renderer>().material = blue;
        }
        else
        {
            GetComponent<Renderer>().material = green;
        }
    }
    public void Touch()
    {
        lifes--;
        if(lifes <= 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            ChangeColor();
        }
    }
    // Update is called once per frame
    void Update() {
        
    }
}
