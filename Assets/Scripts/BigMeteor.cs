﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigMeteor : TinyMeteor
{


    protected override void Explode()
    {
        MeteorManager.instance.LaunchMeteor(2, transform.position, new Vector2(2, -2), 20);
        MeteorManager.instance.LaunchMeteor(2, transform.position, new Vector2(-2, -2), 20);
        base.Explode();
    }
}

