﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMeteor : TinyMeteor {


    protected override void Explode()
    {
        MeteorManager.instance.LaunchMeteor(0, transform.position, new Vector2(2, -2), 20);
        MeteorManager.instance.LaunchMeteor(0, transform.position,new Vector2(-2,-2),20);
        base.Explode();
    }
}
