﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float vel;
    public float maxX;
    private float axis;
    private Vector3 positionActual;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        axis = Input.GetAxis("Horizontal");
        transform.Translate(vel * axis * Time.deltaTime, 0, 0);
        positionActual = transform.position;
        if(positionActual.x > maxX)
        {
            positionActual.x = maxX;
        }
        else if(positionActual.x < -maxX)
        {
            positionActual.x = -maxX;
        }
        transform.position = positionActual;
    }
}