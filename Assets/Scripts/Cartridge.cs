﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cartridge {
    // Use this for initialization
    public Shot[] shots;
    public int currentShot=0;
    public Cartridge(GameObject shot,Transform posIni,int numShoot)
    {
        shots = new Shot[numShoot];
        for(int i = 0; i < numShoot; i++)
        {
            Vector2 spawnPos = posIni.position;
            spawnPos.x -= i * 0.2f;
            GameObject tmpbullet = GameObject.Instantiate(shot, spawnPos, Quaternion.identity, posIni);
            tmpbullet.name = "Bullet_" + i;
            shots[i] = tmpbullet.GetComponent<Shot>();
        }
    }
    public Shot GetShoot()
    {
        if(currentShot > shots.Length - 1)
        {
            currentShot = 0;
        }

        return shots[currentShot++];
    }
}
